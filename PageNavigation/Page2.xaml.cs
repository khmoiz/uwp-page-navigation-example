﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace PageNavigation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Page2 : Page

    {
        string cache;
        public Page2()
        {

            this.InitializeComponent();
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            
            this.Frame.Navigate(typeof(Page1));

        }
        private void HyperlinkButton_MainPage(object sender, RoutedEventArgs e)
        {
            
            this.Frame.Navigate(typeof(MainPage), cache);

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is string && !string.IsNullOrWhiteSpace((string)e.Parameter))
            {
                string temp = e.Parameter.ToString();
                greeting.Text = $"Hi, {temp}. Have a good day!";
                cache = temp;
            }
            else
            {
                greeting.Text = "Hello World!";
            }
            base.OnNavigatedTo(e);
        }

        private void HyperlinkButton_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
